<?php

namespace App;

use Jenssegers\Mongodb\Model as Moloquent;

class User extends  Moloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'users';

}
